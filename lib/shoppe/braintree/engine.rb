module Shoppe
  module Braintree
    class Engine < Rails::Engine
      initializer 'shoppe.braintree.initializer' do
        Shoppe::Braintree.setup
      end
    end
  end
end
