require 'shoppe/braintree/version'
require 'shoppe/braintree/engine'

module Shoppe
  module Braintree
    class << self
      def merchant_id
        Shoppe.settings.braintree_merchant_id
      end

      def public_key
        Shoppe.settings.braintree_public_key
      end

      def private_key
        Shoppe.settings.braintree_private_key
      end

      def mode
        Shoppe.settings.braintree_mode == 'live' ? 'live' : 'sandbox'
      end

      def setup
        Shoppe.add_settings_group :braintree,
                                  [:braintree_merchant_id,
                                   :braintree_public_key,
                                   :braintree_private_key,
                                   :braintree_mode]

        require 'active_merchant'

        Shoppe::Order.before_acceptance do
          gateway = Shoppe::Braintree.gateway
          payments.where(confirmed: false, method: 'Braintree').each do |payment|
            begin
              gateway.capture(payment.amount * 100, payment.reference)
              payment.update_attribute(:confirmed, true)
            rescue
              raise Shoppe::Errors::PaymentDeclined, 'Payment ##{payment.id} could not be captured by Braintree.'
            end
          end
        end

        Shoppe::Order.before_rejection do
          gateway = Shoppe::Braintree.gateway
          payments.where(confirmed: false, method: 'Braintree').each do |payment|
            gateway.void(payment.reference)
          end
        end
      end

      def gateway
        ActiveMerchant::Billing::Base.mode = :test if mode == 'sandbox'
        ActiveMerchant::Billing::BraintreeGateway.new(
          merchant_id: merchant_id,
          public_key:  public_key,
          private_key: private_key
        )
      end
    end
  end
end
